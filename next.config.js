/** @type {import('next').NextConfig} */
const nextConfig = {

    reactStrictMode: true,
    swcMinify: true,
    images: {
        unoptimized: true
    },
    eslint: {
        ignoreDuringBuilds: true,
    },
    env: {
        BASE_URL: 'http://127.0.0.1:8000/',
    },
    productionBrowserSourceMaps: true,
}

module.exports = nextConfig






