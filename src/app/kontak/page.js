'use client'
import React from 'react'
import Testy from '../comps/Testy/Page'
import { FaHouseUser, FaMailBulk, FaPhoneAlt } from 'react-icons/fa';
import { Zoom } from "react-awesome-reveal";


const page = () => {
  return (
    <Zoom>
      <div className="hero hero-inner">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6 mx-auto text-center">
              <div className="intro-wrap">
                <h1 className="mb-0">Kontak Kami</h1>
                <p className="text-white">Jangan ragu untuk menghubungi kami untuk informasi lebih lanjut atau reservasi. Kami siap membantu Anda mewujudkan perjalanan impian Anda!</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="untree_co-section">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 mb-5 mb-lg-0">
              <form className="contact-form" data-aos="fade-up" data-aos-delay="200">
                <div className="row">
                    <div className="mb-3">
                      <label className="text-black form-label" for="fname">Nama Lengkap</label>
                      <input type="text" className="form-control" id="fname" />
                    </div>
                </div>
                <div className="mb-3">
                  <label className="text-black form-label" for="email">Email</label>
                  <input type="email" className="form-control" id="email" />
                </div>

                <div className="mb-3">
                <label className="text-black form-label form-label" for="message">Pesan</label>
                  <textarea name="" className="form-control" id="message" cols="30" rows="5"></textarea>
                </div>

                <button type="submit" className="btn btn-primary">Kirim</button>
              </form>
            </div>
            <div className="col-lg-5 ml-auto">
              <div className="quick-contact-item d-flex align-items-center mb-4">
                <span className="flaticon-house"><FaHouseUser className='mt-3'/></span>
                <address className="text">
                  Jl.Tgh Ibrahim khalidi, Kediri ,NTB, Indonesia
                </address>
              </div>
              <div className="quick-contact-item d-flex align-items-center mb-4">
                <span className="flaticon-phone-call"><FaPhoneAlt className='mt-3' /></span>
                <address className="text">
                  +62 8786 037 1458
                </address>
              </div>
              <div className="quick-contact-item d-flex align-items-center mb-4">
                <span className="flaticon-mail"><FaMailBulk className='mt-3' /></span>
                <address className="text">
                  Shakatour2021@gmail.com
                </address>
              </div>
            </div>
          </div>
        </div>
      </div>

    </Zoom>
    
  )

}

export default page