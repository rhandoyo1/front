export default function NotFound() {
    return (
      <div>
        <h1 className="text-center mt-3">404 - Page Not Found</h1>
      </div>
    )
  }