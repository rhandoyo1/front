import React from 'react';
import ListPackage from './ListPackage';

const getPackage = async () => {
    try {
        const res = await fetch(`https://www.apishaka.site/api/cat-package`, { cache: 'no-store' });
        const data = await res.json();

        return data
    } catch (error) {
        console.error('Error fetching data:', error);
    }
};

const Detail = async() => {
    const pkt = await getPackage();

    return (<ListPackage packages={pkt}/>)

}

export default Detail