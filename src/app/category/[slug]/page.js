'use client'
import Newsletter from '@/app/comps/Newsletter';
import React from 'react'
import Detail from './comps/Detail';
import Breadcrumb from '@/app/comps/Breadcrumb';


const page = () => {

  return (
    <>
        <Breadcrumb />
        <Detail />
        <Newsletter />
    </>
  )

}

export default page