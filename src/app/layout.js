import { Suspense } from 'react'
import Footer from './comps/Footer'
import Navbars from './comps/Navbars'
import './globals.css'
import 'bootstrap/dist/css/bootstrap.css'
import Loading from './Loading'
import ChatWa from './comps/ChatWa'
import Head from 'next/head'
import Link from 'next/link'

import { SpeedInsights } from "@vercel/speed-insights/next"
import Script from 'next/script'
// own css files here

const URL = process.env.BASE_URL;


export const metadata = {
  title: 'Shaka Tour - Jelajahi Destinasi  Wisata Terbaik di Pulau Lombok bersama kami',
  description: 'Jelajahi pesona tersembunyi Pulau Lombok bersama Shaka Tour. Temukan pengalaman wisata tak terlupakan dengan paket perjalanan eksklusif kami yang dirancang untuk memenuhi kebutuhan petualangan Anda. Mulai dari pesisir pantai yang menakjubkan hingga keindahan alam yang menakjubkan, nikmati liburan Anda dengan layanan unggulan kami. Temukan destinasi wisata terbaik dan rencanakan petualangan Anda bersama Shaka Tour hari ini',
  ogUrl: 'https://lombokshakatour.com/',
  ogType: 'website',
  ogImage: 'https://ogcdn.net/6064b869-74ed-4eb9-b76c-0b701ffe7e6b/v4/lombokshakatour.com/Home%20%7C%20shaka%20tour/https%3A%2F%2Fopengraph.b-cdn.net%2Fproduction%2Fdocuments%2Fc24eef12-50fd-4adf-86f8-13824bddc6f5.png%3Ftoken%3D5FGTa1V_LN4cv6xQOo7VXqaTVZ7M8LvPo40DlJaRCkM%26height%3D119%26width%3D136%26expires%3D33245858255/og.png', // Ganti dengan URL gambar Open Graph Anda
  twitterCard: 'summary_large_image',
}

export default function RootLayout({ children }) {

  return (
    <html lang="en">
      <Head>
        <title>{metadata.title}</title>
        <meta name="description" content={metadata.description} />
        <meta property="og:title" content={metadata.title} />
        <meta property="og:description" content={metadata.description} />
        <meta property="og:type" content={metadata.ogType} />
        <meta property="og:image" content={metadata.ogImage} />
        <meta property="og:url" content={metadata.ogUrl} />
        <meta name="twitter:card" content={metadata.twitterCard} />
        <meta name="twitter:description" content={metadata.description} />
        <meta name="twitter:title" content={metadata.title} />
        <meta name="twitter:image" content={metadata.ogImage} />

        <Link rel="apple-touch-icon" sizes="180x180" href={'../../public/icon/apple-touch-icon.png'} />
        <Link rel="icon" type="image/png" sizes="32x32" href="../../public/icon/favicon-32x32.png" />
        <Link rel="icon" type="image/png" sizes="16x16" href="../../public/icon/favicon-16x16.png" />
        <Link rel="manifest" href="../../public/icon/site.webmanifest" />

      </Head>
      <body>
        <Navbars />
        <Suspense fallback={<Loading />}>
          {children}
          <SpeedInsights />
        </Suspense>
        <Footer />

        <ChatWa />

        <Script src="https://code.jquery.com/jquery-3.6.0.min.js"></Script>
        {/* <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.10.2/umd/popper.min.js"></script> */}
        <Script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></Script>

        <Script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></Script>
      </body>
    </html>
  )
}



