import React from 'react';
import { PropagateLoader } from 'react-spinners';

const Loading = () => {
  return (
    <div style={{
      position: 'fixed',
      top: '0',
      left: '0',
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      zIndex: '9999'
    }}>
      <div style={{
        textAlign: 'center'
      }}>

        <PropagateLoader color="#36d7b7" />
      </div>

    </div>

  )

}

export default Loading