import React from 'react';
import Image from 'next/image';

const getDetail = async(slug)=>{
    const URL = process.env.BASE_URL;


    const res = await fetch(`${URL}/api/blog/${slug}`, { cache: 'no-store' })
    return res.json()
}


const DetailBlog = async({slug}) => {
    const blog = await getDetail(slug)
    
    return (
        <>
            <h1>{blog.title}</h1>
            <div className='image-blog mb-3'>
                <Image src={blog.image} width={0} height={0} style={{ width: '100%', height: 'auto' }} />
            </div>
            <div dangerouslySetInnerHTML={{ __html: blog.content }} />
        </>
    )

}

export default DetailBlog