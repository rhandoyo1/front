// 'use client'
import React from 'react';
import Paket from './Paket';
import { useParams } from 'next/navigation';
import DetailBlog from './DetailBlog';
import Breadcrumb from '@/app/comps/Breadcrumb';


export const generateMetadata=({ params})=> {
    const slug = params.slug
   
    return {
      title: `blog/detail/${slug}`,
     
    }
  }
   


const page = ({params}) => {
    const slug  = params.slug;

    return (
        <>
        <Breadcrumb />
            <div className="container">
                <div className='row mt-4'>
                    <div className='col-lg-8'>
                        <DetailBlog slug={slug} />
                    </div>
                    <div className='col-lg-4'>
                        <Paket />
                    </div>
                </div>
            </div>
        </>
    )

}

export default page