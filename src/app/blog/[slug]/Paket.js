'use client'
import React, { useState, useEffect } from 'react';
import Image from 'next/image';
import Link from 'next/link';


const paket = () => {
    const URL = process.env.BASE_URL;

    const [ paket, setPaket] = useState([])

    useEffect(()=>{
        getData()
    },[])


    const getData = async () => {
        try {
          const res = await fetch(`${URL}/api/cat-package`, { cache: 'no-store' });
          const data = await res.json();
    
          setPaket(data);
        } catch (error) {
          console.error('Error fetching data:', error);
        }
      };

    return (
        <>
            <h1>Paket wisata</h1>
            <hr />
            {/* paket wisata */}
            {
                paket&&paket.map((item,i)=>(
                <div className="card mb-3" style={{
                    maxWidth: '540px'
                }}>
                    <Link href={`/paket/${item.slug}`}>
                        <div className="row no-gutters">
                            <div className="col-md-4 col-4">
                                <Image src={item.image} height={110} width={120} className="card-img" alt={item.name} />
                            </div>
                            <div className="col-md-8 col-8">
                                <div className="card-body">
                                    <h5 className="card-title">
                                        {item.name}
                                    </h5>
                                    <p className="card-text">Rp {item.price}</p>
                                </div>
                            </div>
                        </div>
                    </Link>
                </div>
                ))
            }
        </>
    )

}

export default paket