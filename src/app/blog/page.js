import React from 'react'
import Content from './Content'

const page = () => {
 
  return (
    <>
      <div className="hero hero-inner">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6 mx-auto text-center">
              <div className="intro-wrap">
                <h1 className="mb-0">Blog</h1>
                <p className="text-white">Berikut sekilas tentang blog wisata yang ada di lombok. </p>
              </div>
            </div>
          </div>
        </div>
      </div>

     <Content />

    </>
  )
}

export default page