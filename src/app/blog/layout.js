import React from 'react';

export const metadata = {
    title: 'blog | shaka tour',
  }
  

const layout = ({children}) => {
  return (
    <div>
        {children}
    </div>
  )
}

export default layout