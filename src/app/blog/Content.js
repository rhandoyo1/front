// 'use client'
import React from 'react';
import Link from 'next/link';
import { FaClock } from 'react-icons/fa';
import Image from 'next/image'



const Content = async() => {
    const URL = process.env.BASE_URL;


    const res = await fetch(`${URL}/api/blog-list`,
    { cache: 'no-store' }
    );
    const blogs = await res.json()

    return (
        <>
            <div className="untree_co-section">
                <div className="container">
                    <div className="row">
                        {
                            blogs && blogs.map((item, i) => (

                                <div key={i} className="col-6 col-md-6 col-lg-3">
                                    <div className="media-1">
                                        <Link href={`blog/${item.slug}`} className="d-block mb-3">
                                            <Image src={item.image} width={748} height={1023} alt={`gambar ${item.title}`} className="img-fluid" />
                                        </Link>
                                        <div className="d-flex">
                                            <div>
                                                <h3><Link href={`blog/${item.slug}`}>{item.title}</Link></h3>
                                                <p style={{ color: '#776B5D' }}>
                                                    <i><FaClock style={{ marginRight: '7px', marginBottom: '4px' }} /></i>
                                                    2-3-2003
                                                </p>
                                                <div dangerouslySetInnerHTML={{ __html: item.content.slice(0,100) }} /> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))
                        }

                    </div>
                </div>
            </div>
        </>
    )

}

export default Content