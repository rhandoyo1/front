'use client'
import React from 'react';
import Link from 'next/link';
import { FaApple, FaDribbble, FaEnvelope, FaFacebook, FaGoogle, FaInstagram, FaLinkedinIn, FaMapMarkedAlt, FaPhoneAlt, FaPinterest, FaTwitter } from "react-icons/fa";

const Footer = () => {
  return (
    <>
      <div className="site-footer">
        <div className="inner first">
          <div className="container">
            <div className="row">
              <div className="col-md-6 col-lg-4">
                <div className="widget">
                  <h3 className="heading">Tentang</h3>
                  <p>Kami adalah Mitra Perjalanan Terpercaya Lombok!. Sebagai agen yang berkomitmen untuk menyajikan layanan berkualitas tinggi, kami menawarkan berbagai paket perjalanan yang dirancang untuk memenuhi kebutuhan dan keinginan beragam pelanggan kami.</p>
                </div>
                <div className="widget">
                  <ul className="list-unstyled social">
                    <li className='mx-1'>
                      <Link href="#"><span className="icon-twitter"><FaTwitter /></span></Link>
                    </li>
                    <li className='mx-1'><Link href="#"><span className="icon-instagram"><FaInstagram /></span></Link></li>
                    <li className='mx-1'><Link href="#"><span className="icon-facebook"><FaFacebook /></span></Link></li>
                    <li className='mx-1'><Link href="#"><span className="icon-linkedin"><FaLinkedinIn /></span></Link></li>
                    <li className='mx-1'><Link href="#"><span className="icon-dribbble"><FaDribbble/></span></Link></li>
                    <li className='mx-1'><Link href="#"><span className="icon-pinterest"><FaPinterest/></span></Link></li>
                    <li className='mx-1'><Link href="#"><span className="icon-apple"><FaApple /></span></Link></li>
                    <li className='mx-1'><Link href="#"><span className="icon-google"><FaGoogle /></span></Link></li>
                  </ul>
                </div>
              </div>
              <div className="col-md-6 col-lg-2 pl-lg-5">
                <div className="widget">
                  <h3 className="heading">Paket</h3>
                  <ul className="" style={{
                    listStyleType:'none',
                    paddingLeft:'0 !important'
                  }}>
                    <li><Link href="/category/paket-tour">Paket tour</Link></li>
                    <li><Link href="/category/paket-honeymoon">Heneymoon</Link></li>
                    <li><Link href="/category/paket-harian">Harian</Link></li>
                  </ul>
                </div>
              </div>
              <div className="col-md-6 col-lg-2">
                <div className="widget">
                  <h3 className="heading">Halaman</h3>
                  <ul className="" style={{
                    listStyleType:'none',
                    paddingLeft:'0 !important'
                  }}>
                    <li><Link href="/blog">Blog</Link></li>
                    <li><Link href="/kontak">Kontak</Link></li>
                    <li><Link href="/tentang">Tentang</Link></li>
                  </ul>
                </div>
              </div>
              <div className="col-md-6 col-lg-4">
                <div className="widget">
                  <h3 className="heading">Kontak</h3>
                  <ul className="" style={{
                    listStyleType:'none',
                    paddingLeft:'0 !important'
                  }}>
                    <li>
                      <span><FaEnvelope /></span>
                      <Link className='ms-2' href="#">Shakatour2021@gmail.com</Link>
                    </li>
                    <li>
                      <span><FaPhoneAlt /></span>
                      <Link className='ms-2' href="#">+62 786 0371 458</Link>
                    </li>
                    <li>
                      <span><FaMapMarkedAlt /></span>
                      <Link className='ms-2' href="#">Kediri, Lombok NTB</Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>



        <div className="inner dark">
          <div className="container">
            <div className="row text-center">
              <div className="col-md-8 mb-3 mb-md-0 mx-auto">
                <p>Copyright &copy;shaka tour. &mdash; Designed with love by <Link href="https://untree.co" className="link-highlight">Untree.co</Link> 
                Distributed By <Link href="https://themewagon.com" target="_blank" >
                  ThemeWagon
                </Link>
                </p>
              </div>

            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Footer