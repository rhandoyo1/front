'use client'
import React from 'react'
import { Zoom } from "react-awesome-reveal";


const Transport = () => {
    return (
        <Zoom>
            <div style={{
                background:'rgba(26, 55, 77, 0.05)',
                paddingTop:'30px',
                paddingBottom:'30px'
            }}>
                <div className="container">
                    <div className="row justify-content-center text-center mb-5">
                        <div className="col-lg-6">
                            <h2 className="section-title text-center mb-3">Transportasi Bandara</h2>
                            <p>Budayakanlah malas di jemput, karena kami sudah siap membantu Anda. Hubungi kami dan kami akan menunggu anda.</p>
                        </div>
                    </div>

                    <div className="row">
                        <div className='col-lg-10 mx-auto'>
                            <div className='table-responsive'>
                                <table className="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">Tujuan</th>
                                            <th scope="col">Avanza</th>
                                            <th scope="col">Reborn</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style={{fontSize:'15px'}}>Mataram</td>
                                            <td style={{fontSize:'15px'}}>Rp 180.000</td>
                                            <td style={{fontSize:'15px'}}>Rp 300.000</td>
                                        </tr>
                                        <tr>
                                            <td style={{fontSize:'15px'}}>Kuta area</td>
                                            <td style={{fontSize:'15px'}}>Rp 180.000</td>
                                            <td style={{fontSize:'15px'}}>Rp 300.000</td>
                                            
                                        </tr>
                                        <tr>
                                            <td>Senggigi</td>
                                            <td style={{fontSize:'15px'}}>Rp 300.000</td>
                                            <td style={{fontSize:'15px'}}>Rp 500.000</td>
                                            
                                        </tr>
                                        <tr>
                                            <td>Selong Belanak</td>
                                            <td style={{fontSize:'15px'}}>Rp 250.000</td>
                                            <td style={{fontSize:'15px'}}>Rp 400.000</td>
                                            
                                        </tr>
                                        <tr>
                                            <td style={{fontSize:'15px'}}>Kayangan</td>
                                            <td style={{fontSize:'15px'}}>Rp 500.000</td>
                                            <td style={{fontSize:'15px'}}>Rp 700.000</td>
                                            
                                        </tr>
                                        <tr>
                                            <td>Bangsal</td>
                                            <td style={{fontSize:'15px'}}>Rp 400.000</td>
                                            <td style={{fontSize:'15px'}}>Rp 650.000</td>
                                            
                                        </tr>
                                        <tr>
                                            <td>Sembalun</td>
                                            <td style={{fontSize:'15px'}}>Rp 600.000</td>
                                            <td style={{fontSize:'15px'}}>Rp 900.000</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Zoom>
    )

}

export default Transport