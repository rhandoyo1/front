'use client'
import React from 'react';
import Link from 'next/link';
import { useParams} from 'next/navigation';


const Breadcrumb = () => {
  const params= useParams();
  const linkParam= params.slug.replace('-',' ');
  const titleParams = linkParam.charAt(0).toUpperCase() + linkParam.slice(1)

    return (
        <>
        <div style={{
            background:'#F5F5F5',
            paddingBottom:'30px',
            paddingTop:'30px'
        }}>
            <div className='container'>
                <nav>
                    <ol className="breadcrumb m-0">
                        <li className="breadcrumb-item"><Link style={{ color: '#3A3A3A' }} href="/">Home</Link></li>
                        <li className="breadcrumb-item active" aria-current="page" style={{ color: '#3A3A3A' }}>{titleParams}</li>
                    </ol>
                </nav>
            </div>

        </div>
        </>
    )

}

export default Breadcrumb