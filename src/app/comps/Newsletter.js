'use client'
import React from 'react'

const Newsletter = () => {
  return (
    <>
        <div className="py-5 cta-section">
		<div className="container">
			<div className="row text-center">
				<div className="col-md-12">
					<h2 className="mb-2 text-white">Sebagai pelanggan setia newsletter kami </h2>
					<p className="mb-4 lead text-white text-white-opacity">Jangan lewatkan kesempatan ini! Langsung kirim email anda dan konsultasi dengan kami. Terima kasih sudah menjadi bagian dari komunitas perjalanan kami!</p>
					{/* <p className="mb-0"><a href="#" className="btn btn-outline-white text-white btn-md font-weight-bold">Get in touch</a></p> */}
					<div className='mx-auto w-50'>
						<input type='email' placeholder='Masukkan email utama' className='form-control' />
					</div>
				</div>
			</div>
		</div>
	</div>
    </>
  )

}

export default Newsletter