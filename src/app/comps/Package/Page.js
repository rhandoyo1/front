'use client'
import React, { useEffect, useState } from 'react';
import GetPackage from './GetPackage';
import Link from 'next/link';
import Image from 'next/image';
import { FaLocationDot } from 'react-icons/fa6';
import Loading from '@/app/Loading';
import { Zoom } from "react-awesome-reveal";

const Page = ({category}) => {

  const [packageData, setPackageData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await GetPackage();
        setPackageData(data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []); 

  
  const PaketCategory = packageData.filter(item=>item.category == category);
  
    const pkt =()=>{
        if(PaketCategory.length>0){
            return (PaketCategory)
        }else{
            return(packageData)
        }
    }


    return (
      <Zoom>
      
      {

        packageData?(
          <div className="untree_co-section">
              <div className="container">
                  <div className="row justify-content-center text-center mb-5">
                      <div className="col-lg-6">
                          <h2 className="section-title text-center mb-3">Paket wisata</h2>
                          <p>Nikmati momen tanpa khawatir dengan paket wisata khusus kami! Destinasi menakjubkan, pengalaman lokal yang autentik, dan layanan terbaik. Buat kenangan tak terlupakan bersama kami!</p>
                      </div>
                  </div>

                  <div className="row">
                      {
                            pkt().map((item, i) => (
                                <div key={i} className="col-6 col-sm-6 col-md-6 col-lg-3 mb-4 mb-lg-0">
                                    <div className="media-1" style={{
                                        marginBottom:'2em'
                                    }}>
                                        <Link href={`paket/${item.slug}`} className="d-block mb-3">
                                            <Image src={item.image} 
                                                alt={`gambar paket ${item.name}`} 
                                                className="img-fluid" 
                                                width={700} 
                                                height={800}
                                            />
                                        </Link>
                                        <span className="d-flex align-items-center loc mb-2">
                                            <span className="icon-room me-3" style={{
                                                marginBottom: '5px'
                                            }}><FaLocationDot style={{
                                                width: '8px'
                                            }} /></span>
                                            <span style={{fontSize:'14px'}}>Lombok</span>
                                        </span>
                                        <div className="d-flex align-items-center">
                                            <div>
                                                <h3><Link href={`paket/${item.slug}`}>{item.name}</Link></h3>
                                                <div className="price ml-auto">
                                                    <span>Rp {item.price}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
  
                            ))
                      
                      } 
                  </div>

              </div>
          </div>

        ):(<Loading />)
      }

      </Zoom>
  )
}

export default Page