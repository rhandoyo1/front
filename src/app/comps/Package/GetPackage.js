const GetPackage = async () => {

    const URL = process.env.BASE_URL;

    try {
      const res = await fetch(`${URL}/api/cat-package`, { cache: 'no-store' });
      const data = await res.json()

      return data

    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

export default GetPackage