'use client'
import React from 'react';
import Slider from "react-slick";
import Image from 'next/image';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Link from 'next/link';
import { Zoom } from "react-awesome-reveal";


const Populer = () => {

	const settings = {
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
			{
			  breakpoint: 1024,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
				dots: true
			  }
			},
			{
			  breakpoint: 600,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
				initialSlide: 2
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			  }
			}
		]
	};

	return (
		<Zoom>
			<div className="populer">
				<div className="container">
					<div className="row text-center justify-content-center mb-5">
						<div className="col-lg-7"><h2 className="section-title text-center">Destinasi Populer</h2></div>
					</div>
					<Slider {...settings}>
						<div className="item px-2">
							<Link className="media-thumb" href="#" data-fancybox="gallery">
								<div className="media-text">
									<h3>Gili Trawangan</h3>
									<span className="location">Lombok utara</span>
								</div>
								<Image src="images/populer/gili-air.webp" width={748} height={1023} alt="gambar-gili-air" className="img-fluid" />
							</Link>
						</div>
						<div className="item px-2">
							<Link className="media-thumb" href="#" data-fancybox="gallery">
								<div className="media-text">
									<h3>Gili Meno</h3>
									<span className="location">Lombok Utara</span>
								</div>
								<Image src="images/populer/gili -meno.webp" width={748} height={1023}  alt="gambar-gili-meno" className="img-fluid" />
							</Link>
						</div>
						<div className="item px-2">
							<Link className="media-thumb" href="#" data-fancybox="gallery">
								<div className="media-text">
									<h3>Gili Air</h3>
									<span className="location">Lombok Utara</span>
								</div>
								<Image src="images/populer/gili-air.webp" width={748} height={1023} alt="gambar-gili-trawangan" className="img-fluid" />
							</Link>
						</div>
						<div className="item px-2">
							<Link className="media-thumb" href="#" data-fancybox="gallery">
								<div className="media-text">
									<h3>Air Terjun Benang Kelambu</h3>
									<span className="location">Lombok Tengah</span>
								</div>
								<Image src="images/populer/benang-kelambu.webp" width={748} height={1023} alt="gambar-air-terjun-benang-kelambu" className="img-fluid" />
							</Link>
						</div>

						<div className="item px-2">
							<Link className="media-thumb" href="#" data-fancybox="gallery">
								<div className="media-text">
									<h3>Mandalika</h3>
									<span className="location">Lombok Tengah</span>
								</div>
								<Image src="images/populer/mandalika.webp" width={748} height={1023} alt="gambar-mandalika" className="img-fluid" />
							</Link>
						</div>

						<div className="item px-2">
							<Link className="media-thumb" href="#" data-fancybox="gallery">
								<div className="media-text">
									<h3>Pantai Pink</h3>
									<span className="location">Lombok Timur</span>
								</div>
								<Image src="images/populer/pink-2.webp" width={748} height={1023} alt="gambar-pantai-pink" className="img-fluid" />
							</Link>
						</div>
					</Slider>

				</div>
			</div>

		</Zoom>
	)

}

export default Populer