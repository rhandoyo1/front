import React from 'react';
import { FaHeadphones, FaMoneyCheck, FaShieldAlt } from 'react-icons/fa';
import { MdLocalCarWash } from 'react-icons/md';
import { Zoom } from "react-awesome-reveal";


const Whyme = () => {
    return (
        <Zoom>
            <div className="untree_co-section">
                <div className="container">
                    <div className="row mb-5 justify-content-center">
                        <div className="col-lg-6 text-center">
                            <h2 className="section-title text-center mb-3">Kenapa Memilih Kami ?</h2>
                            <p>Keamanan, kenyamanan dan kepuasan pelanggan yang lebih kami utamakan</p>
                        </div>
                    </div>
                    <div className="row align-items-stretch">
                        <div className="col-lg-4 order-lg-1">
                            <div className="" style={{height:'100%'}}><div className="frame" style={{
                                height:'100%'
                            }}><div className="feature-img-bg h-100"
                                style={{
                                    backgroundImage: "url('images/populer/gili -meno.webp')"
                                }}
                            >
                            </div></div></div>
                        </div>

                        <div className="col-6 col-sm-6 col-lg-4 feature-1-wrap d-md-flex flex-md-column order-lg-1" >

                            <div className="feature-1 d-md-flex">
                                <div className="align-self-center">
                                    <span className="flaticon-house display-4">
                                        <FaMoneyCheck className='mb-2' style={{color:'#1A374D'}} />
                                    </span>
                                    <h3>Murah</h3>
                                    <p className="mb-0">Dapatkan pengalaman perjalanan dengan harga terjangkau dengan kami</p>
                                </div>
                            </div>

                            <div className="feature-1 ">
                                <div className="align-self-center">
                                    <span className="flaticon-restaurant display-4">
                                        <FaShieldAlt className='mb-2' style={{color:'#1A374D'}}/>
                                    </span>
                                    <h3>Keamanan</h3>
                                    <p className="mb-0">Sistem Keamanan dan perlindungan pelanggan yang kami gunakan</p>
                                </div>
                            </div>

                        </div>

                        <div className="col-6 col-sm-6 col-lg-4 feature-1-wrap d-md-flex flex-md-column order-lg-3" >

                            <div className="feature-1 d-md-flex">
                                <div className="align-self-center">
                                    <span className="flaticon-mail display-4">
                                        <MdLocalCarWash className='mb-2' style={{color:'#1A374D'}}/>
                                    </span>
                                    <h3>Kebersihan</h3>
                                    <p className="mb-0">Standar kebersihan tertinggi di setiap destinasi</p>
                                </div>
                            </div>

                            <div className="feature-1 d-md-flex">
                                <div className="align-self-center">
                                    <span className="flaticon-phone-call display-4">
                                      <FaHeadphones className='mb-2' style={{color:'#1A374D'}}/>
                                    </span>
                                    <h3>Support</h3>
                                    <p className="mb-0">Tim dukungan pelanggan siap membantu Anda 24 jam</p>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div className="untree_co-section count-numbers py-5">
                <div className="container">
                    <div className="row">
                        <div className="col-6 col-sm-6 col-md-6 col-lg-3">
                            <div className="counter-wrap">
                                <div className="counter">
                                    <span className="" data-number="9313">5</span>
                                </div>
                                <span className="caption">Agen travel</span>
                            </div>
                        </div>
                        <div className="col-6 col-sm-6 col-md-6 col-lg-3">
                            <div className="counter-wrap">
                                <div className="counter">
                                    <span className="" data-number="8492">12</span>
                                </div>
                                <span className="caption">Langganan</span>
                            </div>
                        </div>
                        <div className="col-6 col-sm-6 col-md-6 col-lg-3">
                            <div className="counter-wrap">
                                <div className="counter">
                                    <span className="" data-number="100">2</span>
                                </div>
                                <span className="caption">Penghargaan</span>
                            </div>
                        </div>
                        <div className="col-6 col-sm-6 col-md-6 col-lg-3">
                            <div className="counter-wrap">
                                <div className="counter">
                                    <span className="" data-number="120">20+</span>
                                </div>
                                <span className="caption">Destinasi</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Zoom>
    )

}

export default Whyme