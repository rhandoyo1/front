'use client'
import React, { useState } from 'react';
import heroBg from '../../../public/hero-bg.webp'


const Hero = ({setCategory}) => {
  const [ cat, setCat] = useState('0')

  const handleSubmit=(e)=>{
    e.preventDefault();
    setCategory(cat)
  }
 
  return (
    <>
      <div className="hero" style={{
        backgroundImage: `url(${heroBg.src})`,
        backgroundPosition:'center',
        backgroundSize:'cover',
      }}>
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-7">
              <div className="intro-wrap">
                <h1 className="mb-5"><span className="d-block">Nikmati Liburan Anda</span>Di Lombok<span className="typed-words"></span></h1>

                <div className="row">
                  <div className="col-12">
                    <form className="form">
                      <div className="row mb-2">
                        <div className='col my-auto'>
                            <h4>Pilih Kategory</h4>
                        </div>
                        <div className="col-sm-12 col-md-6 mb-3 mb-lg-0 col-lg-4">
                          <select name="category" value={cat} defaultValue={'0'} onChange={(e)=> setCat(e.target.value)} id="" className="form-control custom-select">
                            <option value="0">Semua Paket</option>
                            <option value="paket-tour">Paket Tour</option>
                            <option value="paket-harian">Paket Harian</option>
                            <option value="paket-honeymoon">Paket Honeymoon</option>
                          </select>
                        </div>
                        <div className="col-sm-12 col-md-6 mb-3 mb-lg-0 col-lg-4">
                          <input type="button" onClick={handleSubmit} className="btn btn-primary btn-block" value="Cari" />
                        </div>
                      </div>
                     
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )

}

export default Hero