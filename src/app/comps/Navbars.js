'use client'
import React, { useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import Logo from '../../../public/logo2.png';
import SideMenu from './SideMenu';
import { FiAlignRight } from "react-icons/fi";



const Navbars = () => {
  const [showMenu, setShowMenu] = useState(false);
  const [isShow, setIsShow] = useState(false);

  return (
    <>
      <SideMenu showMenu={showMenu} setShowMenu={setShowMenu} />
      <nav className="navbar navbar-expand-lg navbar-light bg-default">
        <div className="container">
          <Link className="navbar-brand" href="/">
            <Image src={Logo} width={70} alt='logo shaka tour' />
          </Link>
          <button className="navbar-toggler" style={{
            border:'none'
          }} onClick={() => setShowMenu(!showMenu)} type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <FiAlignRight size={30}/>
          </button>
          <div className="collapse navbar-collapse " id="navbarNavDropdown">
            <ul className="navbar-nav ms-auto">
              <li className="nav-item me-3">
                <Link className="nav-link active" aria-current="page" href="/">Home</Link>
              </li>
              <li className="nav-item dropdown">
                <Link className="nav-link dropdown-toggle" onClick={() => setIsShow(!isShow)} href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="true">
                  Paket
                </Link>
                <ul className={`dropdown-menu ${isShow && 'show'} `} aria-labelledby="navbarDropdownMenuLink">
                  <li><Link className="dropdown-item" href="/category/paket-tour" onClick={()=>setIsShow(false)}>Paket Tour</Link></li>
                  <li><Link className="dropdown-item" href="/category/paket-honeymoon" onClick={()=>setIsShow(false)}>Paket Honeymoon</Link></li>
                  <li><Link className="dropdown-item" href="/category/paket-harian" onClick={()=>setIsShow(false)}>Paket Harian</Link></li>
                </ul>
              </li>
              <li className="nav-item me-3">
                <Link className="nav-link" href="/blog">Blog</Link>
              </li>
              <li className="nav-item me-3">
                <Link className="nav-link" href="/kontak">Kontak</Link>
              </li>
              <li className="nav-item me-3">
                <Link className="nav-link" href="/tentang">Tentang</Link>
              </li>

            </ul>
          </div>
        </div>
      </nav>
    </>
  )

}

export default Navbars