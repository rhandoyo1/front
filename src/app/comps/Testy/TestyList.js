'use client'
import React from 'react';
import Slider from 'react-slick';
import Image from 'next/image';


const TestyList = ({datas}) => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
      };
    
    return (
        <>
            <Slider {...settings}>
                {datas.map((item) => (
                    <div key={item.id} className="testimonial mx-auto">
                        <figure className="img-wrap">
                            <Image src={item.image} width={800} height={800} alt="img-testy" className="img-fluid" />
                        </figure>
                        <h3 className="name">{item.name}</h3>
                        <blockquote>
                            <p>&ldquo;{item.komentar}&rdquo;</p>
                        </blockquote>
                    </div>
                ))}
            </Slider>
        </>
    )
}

export default TestyList