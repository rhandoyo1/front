import React from 'react';
import TestyList from './TestyList';


const GetData = async () => {
  try {
    const res = await fetch('https://www.apishaka.site/api/testimony-list',
    { cache: 'no-store' }
    );
    const data = await res.json()
    return data

  } catch (error) {
    console.error('Error fetching data:', error);
  }
};


const Page = async() => {
  const datas = await GetData();

  return (
    <>
      <div className="untree_co-section testimonial-section mt-5">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-7 text-center">
              <h2 className="section-title text-center mb-5">Apa kata pelanggan kami ?</h2>

              <div className="owl-single owl-carousel no-nav">
                <TestyList datas={datas} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Page;
