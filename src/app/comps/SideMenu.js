'use client'
import React, { useState } from 'react';
import Link from 'next/link';


const SideMenu = ({ showMenu, setShowMenu }) => {
  const [isShow, setIsShow] = useState(false);

  return (
    <>
      <div style={{ width: '100%', backgroundColor:'#6998AB' }} className={`offcanvas offcanvas-end ${showMenu && 'show'}`}  id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
        <div className="offcanvas-header">
         
          <button type="button" onClick={() => setShowMenu(false)} className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div className="offcanvas-body" style={{
          color:'#FFFFFF'
        }}>
          <div className="collapse navbar-collapse show" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <hr/>
              <li className="nav-item">
                <Link className="nav-link" onClick={() => setShowMenu(false)} aria-current="page" href="/">Home</Link>
              </li>
              <hr/>
              <li className="nav-item dropdown">
                <Link className="nav-link dropdown-toggle" onClick={() => setIsShow(!isShow)} href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="true">
                  Paket
                </Link>
                <ul className={`dropdown-menu ${isShow && 'show'} `} aria-labelledby="navbarDropdownMenuLink">
                  <li><Link className="dropdown-item" href="/category/paket-tour" onClick={() => setShowMenu(false)}>Paket Tour</Link></li>
                  <li><Link className="dropdown-item" href="/category/paket-honeymoon" onClick={() => setShowMenu(false)}>Paket Honeymoon</Link></li>
                  <li><Link className="dropdown-item" href="/category/paket-harian" onClick={() => setShowMenu(false)}>Paket Harian</Link></li>
                </ul>
              </li>
              <hr/>

              <li className="nav-item">
                <Link className="nav-link" onClick={() => setShowMenu(false)} href="/blog">Blog</Link>
              </li>
              <hr/>

              <li className="nav-item">
                <Link className="nav-link" onClick={() => setShowMenu(false)} href="/kontak">Kontak</Link>
              </li>
              <hr/>

              <li className="nav-item">
                <Link className="nav-link" onClick={() => setShowMenu(false)} href="/tentang">Tentang</Link>
              </li>

            </ul>
          </div>
        </div>
      </div>
    </>
  )

}

export default SideMenu