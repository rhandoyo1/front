'use client'
import Link from 'next/link'
import React from 'react';
import Image from 'next/image';
import { Zoom } from "react-awesome-reveal";

const Documentasi = () => {
  return (
    <Zoom>
        <div className="untree_co-section">
		<div className="container">
			<div className="row justify-content-between align-items-center">
				
				<div className="col-lg-6">
					<figure className="img-play-video">
						<Link id="play-video" className="video-play-button" href="https://www.youtube.com/watch?v=mwtbEGNABWU" data-fancybox>
							<span></span>
						</Link>
						<Image src="islamic.webp" width={1920} height={1270} alt="img-islamic-center-lombok" className="img-fluid rounded-20" />
					</figure>
				</div>

				<div className="col-lg-5">
					<h2 className="section-title text-left mb-4">Video Dokumentasi</h2>
					<p>Siapkan diri Anda untuk petualangan visual yang mengesankan! Tonton video dokumentasi terbaru kami dan temukan alasan untuk merencanakan perjalanan Anda berikutnya!</p>

					<ul className="list-unstyled two-col clearfix">
						<li>Wisata pantai</li>
						<li>Bukit</li>
						<li>Air Terjun</li>
						<li>Gunung</li>
						<li>Snorkling</li>
						<li>Perjalanan</li>
						<li>Keluarga</li>
						<li>Grup</li>
						<li>Honeymoon</li>
						<li>Dll</li>
					</ul>
					<p><Link href="#" className="btn btn-primary">Tonton Sekarang</Link></p>
				</div>
			</div>
		</div>
	</div>
    </Zoom>
  )

}

export default Documentasi