'use client'
import Hero from './comps/Hero'
import Populer from './comps/Populer'
import Package from './comps/Package/Page'
import Whyme from './comps/Whyme'
import Testy from './comps/Testy/Page'
import Documentasi from './comps/Documentasi'
import Newsletter from './comps/Newsletter'
import { useState } from 'react';

import Transport from './comps/Transport'


export default function Home() {
  const [ category, setCategory] = useState('0');
  const [ isLoading, setIsLoading] = useState(true);

  
  return (
    <>
    {/* <Loading /> */}
  
      <main>
        <Hero setCategory={setCategory} />
        <Populer />
        <Package category={category} />
        <Transport />
        <Whyme />
        {/* <Testy /> */}
        <Documentasi />
        <Newsletter />
      </main> 
    </>

    
  )

}
