import React from 'react'
import Newsletter from '@/app/comps/Newsletter'
import Detail from './comps/Detail';

export const generateMetadata=({ params})=> {
    const slug = params.slug
   
    return {
      title: `detail paket ${slug}`,
     
    }
  }
   

const page = ({params}) => {
    const slug = params.slug;
    return (
        <>
            <Detail slug={slug} />
            <Newsletter />
        </>
    )

}

export default page