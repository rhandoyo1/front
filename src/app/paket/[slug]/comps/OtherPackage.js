import React from 'react';
import Link from 'next/link';
import { FaLocationDot } from 'react-icons/fa6';
import Image from 'next/image';

const getData = async () => {

    const URL = process.env.BASE_URL;

    try {
        const res = await fetch(`${URL}/api/cat-package`, { cache: 'no-store' });
        const data = await res.json();

        return data
    } catch (error) {
        console.error('Error fetching data:', error);
    }
};

const OtherPackage = async({ id }) => {
    const pakets = await getData()
    const other = pakets.filter(pkt => pkt.id !== id)

    return (
        <>
            <div className="row justify-content-center mt-5 section">

                <div className="col-lg-10">
                    <div className="row mb-5">
                        <div className="col text-center">
                            <h2 className="section-title text-center">Paket Lainnya</h2>
                        </div>
                    </div>
                    <div className="row">
                        {
                            other&&other.map((item,i)=>(
                              
                                <div key={i} className="col-6 col-sm-6 col-md-6 col-lg-3 mb-4 mb-lg-0">
                                    <div className="media-1" style={{
                                        marginBottom:'2em'
                                    }}>
                                        <Link href={`${item.slug}`} className="d-block mb-3"><Image src={item.image} width={700} height={800} alt={`gambar paket ${item.name}`} className="img-fluid" /></Link>
                                        <span className="d-flex align-items-center loc mb-2">
                                        <span className="icon-room me-3" style={{
                                                marginBottom: '5px'
                                            }}><FaLocationDot style={{
                                                width: '8px'
                                            }} /></span>
                                            <span style={{fontSize:'14px'}}>Lombok</span>
                                        </span>
                                        <div className="d-flex align-items-center">
                                            <div>
                                                <h3><Link href={`${item.slug}`}>{item.name}</Link></h3>
                                                <div className="price ml-auto">
                                                    <span>Rp {item.price}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            ))
                        }


                    </div>
                </div>
            </div>
        </>
    )

}

export default OtherPackage