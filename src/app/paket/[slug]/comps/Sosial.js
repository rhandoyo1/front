'use client'
import React from 'react';
import Link from 'next/link';
import { FaTelegram, FaWhatsapp } from 'react-icons/fa';

const Sosial = ({pkt}) => {


    return (
        <>
            <div className="custom-block" data-aos="fade-up">
                <h2 className="section-title">Pesan Paket</h2>
                <ul className="list-unstyled social-icons">
                    <li><Link style={{
                        background:'#25D366',
                        color:'#FFFFFF'
                    }} target='_blank' href={`https://wa.me/+6287860371458?text=Hi kak, Mau Pesan Paket ${pkt} dong`}><span className="icon-wa"><FaWhatsapp /></span></Link></li>

                    <li><Link style={{
                        background:'#0088cc',
                        color:'#FFFFFF',
                        marginLeft:'5px'
                    }} href={`https://t.me/+6287860371458`}><span className="icon-tlg"><FaTelegram /></span></Link></li>
                </ul>
            </div>
        </>
    )

}

export default Sosial