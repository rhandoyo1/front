'use client'
import React, { useState } from 'react'
import { useForm } from "react-hook-form"
import Link from 'next/link';
import { useRouter } from "next/navigation";
import { FaFacebook, FaInstagram, FaTwitter } from 'react-icons/fa';
import Swal from 'sweetalert2'



const Form = ({ packages }) => {
    const { register, handleSubmit, formState: { errors } } = useForm()
    const [selectedDate, setSelectedDate] = useState('');
    const [ isLoading, setIsLoading] = useState(false)
    const [value, setValue] = useState()

    const router = useRouter();

    const tomorrow = new Date();
    tomorrow.setDate(new Date().getDate() + 2); 
    const tomorrowFormatted = tomorrow.toISOString().split('T')[0]; 


    const URL = process.env.BASE_URL;


    const handleDateChange = (event) => {
        setSelectedDate(event.target.value);
    };

    const onSubmit = async(data) => {

        setIsLoading(true)
        data.tanggal_jemput = selectedDate;
        data.status = 0

        if (data.tanggal_jemput) {
            try {
                const res = await fetch(`${URL}/api/booking`, {
                    method: 'POST',
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify(data),
                });

                if (res.ok) {
                    router.push('/')
                    Swal.fire({
                        position: "top-end",
                        icon: "success",
                        title: "Pemesanan Berhasil",
                        showConfirmButton: false,
                        timer: 1500
                      });
                      setIsLoading(false)
                } else {
                    console.log('Error:', res.status, res.statusText);
                    Swal.fire({
                        icon: "error",
                        title: "Oops...",
                        text: "Sedang error",
                        footer: ''
                    });
                    setIsLoading(false)

                }
            } catch (err) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Sedang error!",
                    footer: ''
                });
            }

        } else {
            Swal.fire({
                icon: "error",
                title: "Oops...",
                text: "Tanggal Belum di isi",
                footer: ''
            });
            setIsLoading(false)

        }

    };


    return (
        <>
            <div className="col-lg-4">
                <div className="custom-block fixed" data-aos="fade-up" data-aos-delay="100">
                    <h2 className="section-title">Form Pemesanan</h2>
                    <form className="contact-form bg-white">

                        <div className="mb-3">
                            <label className="text-black" for="fname">Nama Lengkap</label>
                            <input type="text" {...register('name', { required: true, maxLength: 20, minLength: 3 })} className={`form-control ${errors.name && 'is-invalid'}`} id="fname" />
                        </div>

                        <div className="mb-3">
                            <label className="text-black" for="adress">Alamat</label>
                            <input type="text" {...register('address', { required: true, minLength: 5 })} className={`form-control ${errors.address && 'is-invalid'}`} id="adress" />
                        </div>
                        <div className="mb-3">
                            <label className="text-black">No. Hp</label>
                            <input
                                type="tel"
                                pattern="[0-9]*"
                                {...register('phone_number', {
                                    required: true,
                                    minLength: 10,
                                    maxLength: 15,
                                })}
                                className={`form-control ${errors.phone_number && 'is-invalid'}`}
                                id="phone_number"
                            />
                        </div>

                        <div className="mb-3">
                            <label className="text-black" for="jml">Jumlah Peserta</label>
                            <input type="number" {...register('jumlah_peserta', { required: true, min: 1, max: 100 })}
                                className={`form-control ${errors.jumlah_peserta && 'is-invalid'}`}
                                id="jml" />
                        </div>
                        <div className="mb-3">
                            <label className="text-black" for="pkt">Paket Wisata</label>
                            <select className={`form-select ${errors.package && 'is-invalid'}`}
                                {...register("package",{required:true})}>
                                <option selected value="">---Pilih paket---</option>
                                {
                                    packages && packages.map((item, i) => (
                                        <option key={i} value={item.id}>{item.name}</option>
                                    ))
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <label className="text-black" for="date">Tanggal Jemput</label>
                            <input type="date" min={tomorrowFormatted} onChange={handleDateChange} className="form-control" id="date" />
                        </div>
                        <button onClick={handleSubmit(onSubmit)} className='btn btn-primary'
                           style={{pointerEvents: isLoading?'none':''}} type="button">
                           {isLoading?(
                              <div className='spin'>
                                    <div class="spinner-grow my-auto" style={{width:'13px',height:'13px'}}></div>
                                    <div class="spinner-grow my-auto" style={{width:'13px',height:'13px'}}></div>
                                    <div class="spinner-grow my-auto" style={{width:'13px',height:'13px'}}></div>
                              </div>
                           ):('Submit') }
                        </button>
                    </form>
                </div>

                <div className="custom-block" data-aos="fade-up">
                    <h2 className="section-title">Social Icons</h2>
                    <ul className="list-unstyled social-icons light">
                        <li><Link href="#"><span className="icon-facebook"><FaFacebook /></span></Link></li>
                        <li><Link href="#"><span className="icon-twitter"><FaTwitter /></span></Link></li>
                        <li><Link href="#"><span className="icon-linkedin"><FaInstagram /></span></Link></li>
                    </ul>
                </div>
            </div>
        </>
    )

}

export default Form