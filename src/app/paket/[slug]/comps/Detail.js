import React from 'react';
import Image from 'next/image';
import Package from './Package';
import OtherPackage from './OtherPackage';
import Breadcrumb from '@/app/comps/Breadcrumb';

const getDetail = async (slug) => {
    const URL = process.env.BASE_URL;
    try {
        const res = await fetch(`${URL}/api/cat-package-detail/${slug}`,
        { cache: 'no-store' }
        // { next: { revalidate: 300 } }
        );
        const data = await res.json();



        return data

    } catch (error) {
        console.error('Error fetching data:', error);
    }
};


const Detail = async ({ slug }) => {

    const detail = await getDetail(slug);

    return (
        <>
            <Breadcrumb />

            <div className="">
                <div className="container">
                    <div className="mb-5 mt-3">
                        <div className="owl-single dots-absolute owl-carousel">
                            <Image src={detail && detail.image} width={1200} height={500} alt="gambar paket wisata" className="img-fluid" id='img-paket' />
                        </div>
                    </div>

                    <Package packages={detail.package} /> 

                    <OtherPackage id={detail && detail.id} /> 

                </div>
            </div>
        </>
    )

}

export default Detail
