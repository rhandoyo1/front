'use client'
import React from 'react';
import Image from 'next/image';


const Price = ({pkt}) => {
    const URL = process.env.BASE_URL;

    return (
        <>
            <div className="custom-block" data-aos="fade-up">
                <h2 className="section-title">Harga</h2>
				<Image src={`${URL}${pkt.image_price}`} width={748} height={1023} alt='tes' className="img-fluid" />
                
            </div>
        </>
    )

}

export default Price