'use client'
import Loading from '@/app/Loading';
import React from 'react'

const Activity = ({item}) => {

  let items = item&&item.activity;

  console.log('ac', items);

  return (
    <>
        <ul>
            
            {
              items?(
              
                    <div style={{
                      fontSize:'14px'
                    }}>

                    <div dangerouslySetInnerHTML={{__html: items}} />
                      
                    </div>
                
              ):(<Loading/>)
              
            }

        </ul>
    </>
  )
}

export default Activity