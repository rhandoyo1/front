'use client'
import React, {useState} from 'react';
import Activity from './Activity';
import { FaMinus, FaPlus } from 'react-icons/fa';


const styleIcon = {
    width:"10px",
    marginBottom:"4px",
    marginRight:'10px'
}

const Itinerary = ({ pkt }) => {

    const paket = pkt&&pkt;

    const [openAccordions, setOpenAccordions] = useState([]);

    const toggleAccordion = (index) => {
        if (openAccordions.includes(index)) {
        setOpenAccordions(openAccordions.filter((i) => i !== index));
        } else {
        setOpenAccordions([...openAccordions, index]);  
        }
    };


    console.log('pkt',  paket);
    return (
        <div className="custom-block" data-aos="fade-up">
            <h2 className="section-title">Itinerary</h2>
            {
            
                    paket&&paket.itinerary.map((item, i) => (
                        <div key={i} className="custom-accordion" id={`accordion_${i}`}>
                            <div className="accordion-item">
                                <h2 className="mb-0" style={{
                                    fontSize:'12px !important'
                                }}>
                                    <button
                                        className="btn btn-link"
                                        type="button"
                                        data-toggle="collapse"
                                        data-target={`#collapse_${i}`}
                                        aria-expanded="true"
                                        aria-controls={`collapse_${i}`}
                                        onClick={() => toggleAccordion(i)}
                                    >
    
                                     {
                                        openAccordions.includes(i) ? 
                                        <i className="fas fa-minus"><FaMinus style={styleIcon}/></i> : 
                                        <i className="fas fa-plus"><FaPlus style={styleIcon}/></i>
                                     }
                                    
                                     {item.day}
                                    </button>
                                </h2>
    
    
                                <div id={`collapse_${i}`} className="collapse" aria-labelledby={`heading_${i}`} data-parent={`#accordion_${i}`}>
                                    <div className="accordion-body">
                                        <Activity item={item} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))

            
            } 

            {/* baru */}

        </div>
    )
}

export default Itinerary