import React from 'react'
import Form from './Form';
import Itinerary from './Itinerary';
import Gallery from './Gallery';
import Sosial from './Sosial';
import Price from './Price';
import Includes from './Includes';
import Excludes from './Excludes';

const Package = async({ packages }) => {

    const packagess = await packages;


    return (
        <>
            <div className="row justify-content-center">
                <div className="col-lg-4">
                    {
                        packagess && packagess.map((pkt, i) => (
                            <div key={i} className='mb-5 border-bottom'>
                                <div className="custom-block " data-aos="fade-up">
                                    <h1 className="section-title">{pkt.name}</h1>
                                    <div className="des">
                                        <p>{pkt.desc}</p>
                                    </div>
                                </div>

                                <h1>{pkt.image_paket}</h1>

                                <Itinerary pkt={pkt} />

                                <Price pkt={pkt} />

                                <Includes includes={pkt.includes} />

                                <Excludes excludes={pkt.excludes} />

                                <Gallery />

                                <Sosial pkt={pkt.name} />

                            </div>
                        ))
                    }

                </div>
                <Form packages={packages} />
            </div> 
        </>
    )
}

export default Package