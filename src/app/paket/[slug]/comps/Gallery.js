'use client'
import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import Image from 'next/image';

const Gallery = () => {
    const [gallerys, setGallerys] = useState([]);
    const URL = process.env.BASE_URL;


    useEffect(() => {
        getData()
    }, [])

    const getData = async () => {
        try {
            const res = await fetch(`${URL}/api/gallery-list`, { cache: 'no-store' });
            const data = await res.json();

            setGallerys(data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };



    return (
        <>

            <div className="custom-block" data-aos="fade-up">
                <h2 className="section-title">Gallery</h2>
                <div className="row gutter-v2 gallery">

                    {
                        gallerys && gallerys.map((item, i) => (
                            <div key={i} className="col-4">
                                <Link href="#" className="gal-item" data-fancybox="gal"><Image src={item.image} width={955} height={955} alt={item.name} className="img-fluid" /></Link>
                            </div>

                        ))
                    }
                </div>
            </div>
        </>
    )

}

export default Gallery