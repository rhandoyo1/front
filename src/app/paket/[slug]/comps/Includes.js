'use client'
import React from 'react'

const Includes = ({includes}) => {

  return (
    <>
        <div className="custom-block" data-aos="fade-up">
            <h2 className="section-title">Include</h2>
            <ul>
              {
                includes&&includes.map((item, i)=>(
                  <li key={i}>{item.name}</li>

                ))
              }
            </ul>
            
        </div>
    </>
  )

}

export default Includes