import React from 'react'

const Excludes = ({excludes}) => {
  return (
    <>
        <div className="custom-block" data-aos="fade-up">
            <h2 className="section-title">Exclude</h2>
            <ul>
              {
                excludes&&excludes.map((item, i)=>(
                  <li key={i}>{item.name}</li>

                ))
              }
            </ul>
            
        </div>
    </>
  )

}

export default Excludes