'use client'
import React from 'react';
import { Zoom } from "react-awesome-reveal";

const page = () => {
    return (
        <Zoom>
            <div className="hero hero-inner">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-6 mx-auto text-center">
                            <div className="intro-wrap">
                                <h1 className="mb-0">Tentang Kami</h1>
                                {/* <p className="text-white">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="untree_co-section">
                <div className="container">
                    <div className="row">
                        {/* <div className="col-lg-7">
                            <div className="owl-single dots-absolute owl-carousel">
                                <img src="images/slider-1.jpg" alt="Free HTML Template by Untree.co" className="img-fluid rounded-20" />
                            </div>
                        </div> */}
                        <div className="col-lg-12 pl-lg-12 ml-auto">
                            <h2 className="section-title mb-4">Tentang Shaka tour</h2>
                            <p>Selamat datang di Shaka Tour - Mitra Perjalanan Terpercaya Lombok!

                                Kami di Shaka Tour bangga menjadi pilihan utama Anda untuk pengalaman wisata yang tak terlupakan di Lombok. Sebagai agen yang berkomitmen untuk menyajikan layanan berkualitas tinggi, kami menawarkan berbagai paket perjalanan yang dirancang untuk memenuhi kebutuhan dan keinginan beragam pelanggan kami.
                                Berikut jasa yang kami tawarkan :
                            </p>
                            <ul className="list-unstyled two-col clearfix">
                                <li>Paket wisata</li>
                                <li>Paket Honeymoon</li>
                                <li>Paket Harian</li>
                                <li>Antar jemput bandara/Pelabuhan</li>
                                <li>Sewa Mobil</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            {/* <div className="untree_co-section">
                <div className="container">
                    <div className="row justify-content-center mb-5">
                        <div className="col-md-6 text-center">
                            <h2 className="section-title mb-3 text-center">Team</h2>
                            <p>Berikut adalah team dari shaka tour yang tentunya sudah berpengalaman di bidang wisata</p>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-lg-3 mb-4">
                            <div className="team">
                                <img src="images/person_1.jpg" alt="Image" className="img-fluid mb-4 rounded-20" />
                                <div className="px-3">
                                    <h3 className="mb-0">James Watson</h3>
                                    <p>Co-Founder &amp; CEO</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 mb-4">
                            <div className="team">
                                <img src="images/person_2.jpg" alt="Image" className="img-fluid mb-4 rounded-20" />
                                <div className="px-3">
                                    <h3 className="mb-0">Carl Anderson</h3>
                                    <p>Co-Founder &amp; CEO</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-3 mb-4">
                            <div className="team">
                                <img src="images/person_4.jpg" alt="Image" className="img-fluid mb-4 rounded-20" />
                                <div className="px-3">
                                    <h3 className="mb-0">Michelle Allison</h3>
                                    <p>Co-Founder &amp; CEO</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 mb-4">
                            <div className="team">
                                <img src="images/person_3.jpg" alt="Image" className="img-fluid mb-4 rounded-20" />
                                <div className="px-3">
                                    <h3 className="mb-0">Drew Wood</h3>
                                    <p>Co-Founder &amp; CEO</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div> */}

        </Zoom>
    )

}

export default page